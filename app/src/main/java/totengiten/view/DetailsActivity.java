package totengiten.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import totengiten.R;
import totengiten.model.Repo;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        //TODO: injection
        //NOTTODO: data binding library
        Intent intent = getIntent();
        Repo repo = (Repo) intent.getSerializableExtra("Repo");
        TextView t = findViewById(R.id.details_reponame);
        t.setText(repo.getRepoName());
        t = findViewById(R.id.details_username);
        t.setText(repo.getUserName());
        t = findViewById(R.id.details_description);
        t.setText(repo.getDescription());
        ImageView i = findViewById(R.id.details_avatar);
        Picasso.get().load(repo.avatar).into(i);
    }
}
