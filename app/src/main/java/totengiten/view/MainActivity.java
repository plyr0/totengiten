package totengiten.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import totengiten.R;
import totengiten.controller.RowClickListener;
import totengiten.controller.SampleAdapter;
import totengiten.model.ModelConverter;
import totengiten.model.Repo;
import totengiten.rest.bitbucket.BitbucketListener;
import totengiten.rest.bitbucket.BitbucketRepo;
import totengiten.rest.bitbucket.BitbucketRest;
import totengiten.rest.github.GithubListener;
import totengiten.rest.github.GithubRepo;
import totengiten.rest.github.GithubRest;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RowClickListener listener = repo -> {
        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
        intent.putExtra("Repo", repo);
        startActivity(intent);
    };
    private SwipeRefreshLayout refreshLayout;

    private enum SortOrder { BY_NAME, BY_NAME_REVERSED, BY_REPO, BY_REPO_REVERSED }
    SortOrder sortOrder = SortOrder.BY_NAME;

    boolean githubReturned = false;
    boolean bitbucketReturned = false;

    private final String TAG = getClass().getName();
    private SampleAdapter adapter;
    private List<Repo> dataset;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initData();
    }

    private void initData() {
        if (dataset != null){
            dataset.clear();
        } else {
            dataset = new ArrayList<>(200);
        }
        if(adapter == null) {
            adapter = new SampleAdapter(dataset, listener);
            recyclerView.setAdapter(adapter);
        }
        githubReturned = bitbucketReturned = false;
        initGithub();
        initBitbucket();
    }

    private void initBitbucket() {
        new BitbucketRest().requestPublicRepos(new BitbucketListener() {
            @Override
            public void onDelivered(List<BitbucketRepo> values) {
                Log.d(TAG, "Size: " + values.size());
                dataset.addAll(ModelConverter.fromBitBucketRepos(values));
                bitbucketReturned = true;
                dataSetChanged();
            }

            @Override
            public void onFailure(String message) {
                Log.d(TAG, "Error: " + message);
                Toast.makeText(getApplicationContext(), "Error:\n" + message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initGithub() {
        new GithubRest().requestPublicRepos(new GithubListener() {
            @Override
            public void onDelivered(List<GithubRepo> repos) {
                Log.d(TAG, "Size: " + repos.size());
                dataset.addAll(ModelConverter.fromGithubRepos(repos));
                githubReturned = true;
                dataSetChanged();
            }

            @Override
            public void onFailure(String error) {
                Log.d(TAG, "Error: " + error);
                Toast.makeText(getApplicationContext(), "Error:\n" + error, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initView() {
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        refreshLayout = findViewById(R.id.swipe);
        refreshLayout.setOnRefreshListener(this::initData);

        recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refreshLayout.setRefreshing(true);
            initData();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_sort_by_reponame) {
            sortOrder = SortOrder.BY_REPO;
            dataSetChanged();
        }
        else if (id == R.id.nav_sort_by_reponame_reversed) {
            sortOrder = SortOrder.BY_REPO_REVERSED;
            dataSetChanged();
        }
        else if (id == R.id.nav_sort_by_username) {
            sortOrder = SortOrder.BY_NAME;
            dataSetChanged();
        }
        else if (id == R.id.nav_sort_by_username_reversed) {
            sortOrder = SortOrder.BY_REPO_REVERSED;
            dataSetChanged();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void dataSetChanged() {
        switch (sortOrder){
            case BY_NAME:
                Collections.sort(dataset, (o1, o2) -> o1.getUserName().compareToIgnoreCase(o2.getUserName()));
                break;
            case BY_NAME_REVERSED:
                Collections.sort(dataset, (o1, o2) -> o2.getUserName().compareToIgnoreCase(o1.getUserName()));
                break;
            case BY_REPO:
                Collections.sort(dataset, (o1, o2) -> o1.getRepoName().compareToIgnoreCase(o2.getRepoName()));
                break;
            case BY_REPO_REVERSED:
                Collections.sort(dataset, (o1, o2) -> o2.getRepoName().compareToIgnoreCase(o1.getRepoName()));
                break;
        }
        adapter.notifyDataSetChanged();
        if (githubReturned && bitbucketReturned){
            refreshLayout.setRefreshing(false);
        }
    }
}
