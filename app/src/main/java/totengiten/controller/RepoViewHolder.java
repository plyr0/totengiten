package totengiten.controller;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import totengiten.R;
import totengiten.model.Repo;

public class RepoViewHolder extends RecyclerView.ViewHolder {
    public TextView repoName;
    public TextView userName;
    public ImageView avatar;
    public ImageView star;

    public RepoViewHolder(View v) {
        super(v);
        repoName = v.findViewById(R.id.row_reponame);
        userName = v.findViewById(R.id.row_username);
        avatar = v.findViewById(R.id.row_avatar);
        star = v.findViewById(R.id.row_bitbucket);
    }

    public void setItemClickListener(final Repo repo, final RowClickListener listener) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                listener.onRowClick(repo);
            }
        });
    }
}