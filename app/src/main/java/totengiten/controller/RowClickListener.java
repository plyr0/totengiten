package totengiten.controller;

import totengiten.model.Repo;

public interface RowClickListener {
    void onRowClick(Repo repo);
}
