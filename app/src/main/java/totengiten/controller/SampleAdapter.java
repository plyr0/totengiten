package totengiten.controller;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import totengiten.R;
import totengiten.model.Repo;

public class SampleAdapter extends RecyclerView.Adapter<RepoViewHolder> {
    private List<Repo> dataset;
    private RowClickListener listener;

    public SampleAdapter(List<Repo> dataset, RowClickListener listener) {
        this.dataset = dataset;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item, parent, false);
        return new RepoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoViewHolder holder, int position) {
        holder.repoName.setText(dataset.get(position).getRepoName());
        holder.userName.setText(dataset.get(position).getUserName());
        String avatar = dataset.get(position).avatar;
        Picasso.get().load(avatar).into(holder.avatar);
        if(dataset.get(position).isBitbucket){
            holder.star.setVisibility(View.VISIBLE);
        } else {
            holder.star.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RepoViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        holder.setItemClickListener(dataset.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}