package totengiten.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import totengiten.rest.bitbucket.BitbucketRepo;
import totengiten.rest.github.GithubRepo;

public class ModelConverter {

    private static Repo fromGithubRepo(GithubRepo input){
        return new Repo(
                input.name,
                input.owner.login,
                input.owner.avatar_url,
                input.description,
                false
        );
    }

    public static List<Repo> fromGithubRepos(List<GithubRepo> input) {
        List<Repo> repos = new ArrayList<>(input.size());
        for(GithubRepo r : input) {
            repos.add(fromGithubRepo(r));
        }
        return repos;
    }

    public static List<Repo> fromBitBucketRepos(List<BitbucketRepo> values) {
        List<Repo> repos = new ArrayList<>(values.size());
        for(BitbucketRepo r : values) {
            repos.add(fromBitbucketRepo(r));
        }
        return repos;

    }

    private static Repo fromBitbucketRepo(BitbucketRepo input) {
        return new Repo(
                input.name,
                input.owner.username,
                input.owner.links.avatar.href,
                input.description,
                true
        );
    }
}
