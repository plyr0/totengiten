package totengiten.model;

import java.io.Serializable;

public class Repo implements Serializable {

    //TODO custom (gson) demarshalling

    public String repoName;

    public String userName;

    public String avatar;

    public String description;

    public Boolean isBitbucket;


    public Repo() {
    }

    public Repo(String repoName, String userName, String avatar, String description, boolean isBitbucket) {
        this.repoName = repoName;
        this.userName = userName;
        this.avatar = avatar;
        this.description = description;
        this.isBitbucket = isBitbucket;
    }

    public String getRepoName() {
        return repoName;
    }

    public String getUserName() {
        return userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getBitbucket() {
        return isBitbucket;
    }
}
