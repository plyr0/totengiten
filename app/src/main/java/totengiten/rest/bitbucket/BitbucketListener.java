package totengiten.rest.bitbucket;

import java.util.List;

public interface BitbucketListener {
    void onDelivered(List<BitbucketRepo> values);

    void onFailure(String message);
}
