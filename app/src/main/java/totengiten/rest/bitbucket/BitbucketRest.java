package totengiten.rest.bitbucket;

import android.support.annotation.NonNull;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BitbucketRest {

    private final String TAG = getClass().getName();
    private final BitbucketEndpoints service;

    public BitbucketRest() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.bitbucket.org/2.0/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(BitbucketEndpoints.class);
    }

    public void requestMyRepos(final BitbucketListener listener) {
        service.listUserRepos("plyr0").enqueue(getCallback(listener));
    }

    public void requestPublicRepos(final BitbucketListener listener) {
        service.listPublicRepos().enqueue(getCallback(listener));
    }

    private Callback<BitbucketRepoBulk> getCallback(final BitbucketListener listener){
        return new Callback<BitbucketRepoBulk>() {
            @Override
            public void onResponse(@NonNull Call<BitbucketRepoBulk> call,
                                   @NonNull Response<BitbucketRepoBulk> response) {

                if(response.isSuccessful() && response.body() != null) {
                    listener.onDelivered(response.body().values);
                } else {
                    Log.d(TAG, response.toString());
                    listener.onFailure(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BitbucketRepoBulk> call, @NonNull Throwable t) {
                listener.onFailure(t.getMessage());
            }
        };
    }
}
