package totengiten.rest.bitbucket;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

interface BitbucketEndpoints {

    @GET("repositories?pagelen=100")
    Call<BitbucketRepoBulk> listPublicRepos();

    @GET("repositories/{user}")
    Call<BitbucketRepoBulk> listUserRepos(@Path("user") String user);
}
