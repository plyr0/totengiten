package totengiten.rest.bitbucket;

public class BitbucketRepo {

    public String name;

    public Owner owner;
    public class Owner {

        public String username;

        public Links links;
        public class Links{

            public Avatar avatar;
            public class Avatar {

                public String href;
            }
        }
    }

    public String description;
}
