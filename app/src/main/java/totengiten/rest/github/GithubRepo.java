package totengiten.rest.github;

import java.io.Serializable;

public class GithubRepo implements Serializable {

    public String name;

    public Owner owner;
    public class Owner {
        public String login;

        public String avatar_url;
    }

    public String description;
}
