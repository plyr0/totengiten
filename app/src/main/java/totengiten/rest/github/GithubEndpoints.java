package totengiten.rest.github;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GithubEndpoints {

    @GET("repositories")
    Call<List<GithubRepo>> listPublicRepos();

    @GET("users/{user}/repos")
    Call<List<GithubRepo>> listUserRepos(@Path("user") String user);

    //Pagination ?since=id of last repo viewed
}
