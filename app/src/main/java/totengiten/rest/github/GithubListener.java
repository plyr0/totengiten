package totengiten.rest.github;

import java.util.List;

public interface GithubListener {
    void onDelivered(List<GithubRepo> repos);

    void onFailure(String error);
}
