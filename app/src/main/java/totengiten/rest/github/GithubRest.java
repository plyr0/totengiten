package totengiten.rest.github;

import android.support.annotation.NonNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GithubRest {

    private final GithubEndpoints service;

    public GithubRest() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(GithubEndpoints.class);
    }

    public void requestMyRepos(final GithubListener listener) {
        service.listUserRepos("plyr0").enqueue(getCallback(listener));
    }

    public void requestPublicRepos(final GithubListener listener) {
        service.listPublicRepos().enqueue(getCallback(listener));
    }

    private Callback<List<GithubRepo>> getCallback(final GithubListener listener){
        return new Callback<List<GithubRepo>>() {
            @Override
            public void onResponse(@NonNull Call<List<GithubRepo>> call, @NonNull Response<List<GithubRepo>> response) {
                if(response.isSuccessful()) {
                    listener.onDelivered(response.body());
                } else {
                    listener.onFailure(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<GithubRepo>> call, @NonNull Throwable t) {
                listener.onFailure(t.getMessage());
            }
        };
    }
}
